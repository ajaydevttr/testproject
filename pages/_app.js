

import '../styles/globals.css'



import { Amplify } from 'aws-amplify';
import { Authenticator } from 'aws-amplify-react';

Amplify.configure({
    Auth: {
        identityPoolId: 'eu-west-2:3d0219b0-bec8-406e-9f17-cf05fb1d8cf7',
        region: 'eu-west-2',
        userPoolId: 'eu-west-2_0Lh1EXZ1B',
        userPoolWebClientId: '7iro1i9l68092impkb7qmn2fvf',
    },
    Storage: {
        bucket: 'bulkuploadjbcard162057-dev',
        region: 'eu-west-2',
    },
});


function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp