


import { generateUploadURL } from '../../utils/s3'


export const config = {
    api: {
       bodyParser: false,
    }
};



export default async function handler(req, res) {
    // switch the methods
    switch (req.method) {
        case 'GET': {
            return getAwsUrl(req, res);
        }

        case 'POST': {
            return uploadJb(req, res);
        }

      
    }
}



async function getAwsUrl(req, res) {
    const url = await generateUploadURL()
    res.send({ url })
}








