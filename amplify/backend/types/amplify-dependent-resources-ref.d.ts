export type AmplifyDependentResourcesAttributes = {
  "auth": {
    "amplifyprob14515e4": {
      "AppClientID": "string",
      "AppClientIDWeb": "string",
      "IdentityPoolId": "string",
      "IdentityPoolName": "string",
      "UserPoolArn": "string",
      "UserPoolId": "string",
      "UserPoolName": "string"
    }
  },
  "storage": {
    "test": {
      "BucketName": "string",
      "Region": "string"
    }
  }
}