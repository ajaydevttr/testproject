
import S3 from 'aws-sdk/clients/s3'
const { v4: uuidv4 } = require('uuid');
require('dotenv').config();

const region ='eu-west-2'


const ACCSESS_KEY = process.env.ACCSESS_KEY
const SECRET_KEY = process.env.SECRET_KEY
const BUCKET_NAME = process.env.BUCKET_NAME




const s3 = new S3  ({
    region,
    ACCSESS_KEY,
    SECRET_KEY,
    BUCKET_NAME,
    signatureVersion: 'v4'
  })

  export async function generateUploadURL() {
    // const rawBytes = await randomBytes(16)
    let imageName = uuidv4()
    imageName = imageName + 'jb.png'

    const params = ({
      Bucket: BUCKET_NAME,
      Key: imageName,
      Expires: 60 * 5
    })
    const uploadURL = await s3.getSignedUrlPromise('putObject', params)
    return uploadURL

  }

 


  
